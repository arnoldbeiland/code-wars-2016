'use strict';

angular.module('codewars.services')
    .service('productService', [
        '$http',
        '$q',
        '$rootScope',
        'REST_BASE',
        function ($http, $q, $rootScope, REST_BASE) {

            var self = this;

            self.getProducts = function () {
                var defer = $q.defer();

                $http({
                    url: REST_BASE.PRODUCTS,
                    method: 'get'
                }).then(function (result) {
                        defer.resolve(result.data);
                    },
                    function (error) {
                        defer.reject(error);
                    });
                return defer.promise;
            };

            self.getImage = function (id) {
                var defer = $q.defer();

                $http({
                    url: REST_BASE.PRODUCTS + '/images/' + id,
                    method: 'get'
                }).then(function (result) {
                        defer.resolve(result.data);
                    },
                    function (error) {
                        defer.reject(error);
                    });
                return defer.promise;
            };

        }]);
