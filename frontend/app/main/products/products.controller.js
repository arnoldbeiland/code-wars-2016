'use strict';

var productsDetails;
(function () {

    angular.module('codewars2016App')
        .controller('ProductsCtrl', ['$scope', 'productService', '$q', '$uibModal',
            function ($scope, productService, $q, $uibModal) {
                var getProducts, getImages, logos = {};
                getImages = function () {
                    angular.forEach($scope.model.products, function (product) {
                        productService.getImage(product.id).then(function (result) {
                            if (!logos[product.id]) {
                                logos[product.id] = result;
                            }
                            product.picture = logos[product.id];
                        });
                    });
                };

                getProducts = function () {
                    var defer = $q.defer();
                    productService.getProducts().then(function (result) {
                            defer.resolve(result);
                        },
                        function (error) {
                            defer.reject(error);
                        });
                    return defer.promise;
                };

                if (!productsDetails) {
                    $scope.fetchProducts = getProducts();
                }
                else {
                    $scope.fetchProducts = $q.when(productsDetails);
                }

                $q.when($scope.fetchProducts).then(function (result) {
                    productsDetails = angular.copy(result);
                    $scope.model.products = angular.copy(result);
                    getImages();
                });

                $scope.model = {};

                $scope.openWishlistsModal = function (product) {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'main/modal/wishlist-modal/add-to-wishlist/add-to-wishlist-tpl.html',
                        controller: 'AddToWishlistCtrl',
                        size: 'lg',
                        resolve: {
                            product: product
                        }
                    });
                };

            }]);
})();
