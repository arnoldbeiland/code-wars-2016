'use strict';

/**
 * @ngdoc function
 * @name codewars2016App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the codewars2016App
 */
angular.module('codewars2016App')
  .controller('SignupCtrl', [
    '$scope', 'userService', '$location',
    function ($scope, userService, $location) {

      $scope.signup = function () {
        userService.signup($scope.user).then(function () {
          $location.path('/#wishlist');
        });
      };

    }]);
