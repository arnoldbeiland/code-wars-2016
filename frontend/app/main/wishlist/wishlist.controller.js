'use strict';

(function () {

    var wishlistsDetails;

    angular.module('codewars2016App')
        .controller('WishlistCtrl', ['$scope', 'wishlistService', 'userService', '$q', '$uibModal','notificationService',
            function ($scope, wishlistService, userService, $q, $uibModal, notificationService) {
                var getWishlists;

                getWishlists = function () {
                    var defer = $q.defer();
                    wishlistService.getUserWishlists(userService.user.id).then(function (result) {
                        defer.resolve(result);
                    }, function (error) {
                        defer.reject(error);
                    });
                    return defer.promise;
                };

                if (!wishlistsDetails) {
                    $scope.fetchUserWishlists = getWishlists();
                }
                else {
                    $scope.fetchUserWishlists = $q.when(wishlistsDetails);
                }

                $q.when($scope.fetchUserWishlists)
                    .then(function (result) {
                        wishlistsDetails = angular.copy(result);
                        $scope.wishlists = angular.copy(result);
                    });

                $scope.deleteWishlist = function (wishlist, index) {
                    wishlistService.deleteWishList(userService.user.id, wishlist.id).then(function(){
                        $scope.wishlists.splice(index, 1);
                        wishlistsDetails = $scope.wishlists;
                    });
                };

                $scope.openNewWishListModal = function () {

                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'main/modal/wishlist-modal/new-wishlist/new-wishlist-tpl.html',
                        controller: 'NewWishlistCtrl',
                        size: 'lg',
                        resolve: {}
                    });

                    modalInstance.result.then(function () {
                        getWishlists().then(function (result) {
                            $scope.wishlists = result;
                            wishlistsDetails = $scope.wishlists;
                        });
                    });
                };

                $scope.editWishListModal = function (wishlist) {

                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'main/modal/wishlist-modal/edit-wishlist/edit-wishlist-tpl.html',
                        controller: 'EditWishlistCtrl',
                        size: 'lg',
                        resolve: {
                            wishlist: wishlist
                        }
                    });

                    modalInstance.result.then(function () {
                        getWishlists().then(function (result) {
                            $scope.wishlists = result;
                            wishlistsDetails = $scope.wishlists;
                        });
                    });
                };

                notificationService.subscribe('wishlistModified',function (){
                    getWishlists().then(function (result) {
                        wishlistsDetails = result;
                    });
                });
            }]);
})();
