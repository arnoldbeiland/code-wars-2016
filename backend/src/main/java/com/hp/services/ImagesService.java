package com.hp.services;

import com.hp.utils.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class ImagesService {
    private final static Logger LOGGER = Logger.getLogger(ImagesService.class.getName());
    private static ImagesService instance;

    private ImagesService(){}

    public static ImagesService getInstance() {
        if (instance == null) {
            instance = new ImagesService();
        }
        return instance;
    }

    public String findImageNameById(int id) {
        try {
            List<String> images = new ArrayList<>(Arrays.asList(Util.getPath("products/images/").toFile().list()));
            List<String> foundImages =
                    images.stream().filter(s -> s.split("\\.")[0].equals(String.valueOf(id))).collect(Collectors.toList());
            if (!foundImages.isEmpty()) {
                // there should be only one image found
                return foundImages.get(0);
            }
        } catch (URISyntaxException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return "";
    }

    public void deleteImageById(int id) throws URISyntaxException, IOException {
        Files.delete(Util.getPath("products/images/" + findImageNameById(id)));
    }

    public boolean writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) {
        OutputStream out;
        try {
            int read;
            byte[] bytes = new byte[1024];
            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return false;
        }
        return true;
    }
}
