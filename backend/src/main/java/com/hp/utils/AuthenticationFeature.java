package com.hp.utils;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

@Provider
public class AuthenticationFeature implements DynamicFeature {

    private static List<String> noAuthenticationMethods;
    private AuthenticationFilter authenticationFilter = new AuthenticationFilter();

    static {
        noAuthenticationMethods = new ArrayList<>();
        noAuthenticationMethods.add("login");
        noAuthenticationMethods.add("register");
    }

    public void configure(ResourceInfo ri, FeatureContext ctx) {
        if (resourceShouldBeFiltered(ri)) {
            ctx.register(authenticationFilter);
        }
    }

    private boolean resourceShouldBeFiltered(ResourceInfo resourceInfo) {
        return !noAuthenticationMethods.contains(resourceInfo.getResourceMethod().getName());
    }
}
