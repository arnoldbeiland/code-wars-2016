package com.hp.utils;


import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Util {

    public static Path getPath(String uri) throws URISyntaxException {
        Path path = Paths.get(uri);
        if (!path.isAbsolute()) {
            path = Paths.get(Util.class.getProtectionDomain().getCodeSource().getLocation().toURI()).resolve(path);
        }
        return path;
    }

    public static String encryptUsingMD5(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] bytes = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
